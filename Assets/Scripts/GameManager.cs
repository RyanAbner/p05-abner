﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
This script keeps score, generates asteroids, and does other general things.
Inspired by the Unity Space Shooter tutorial's GameController script.
*/

public class GameManager : MonoBehaviour {
    public float spawnWait = 2f;
    public float xBoundary;
    public float zBoundary;
    public float minSpeed;
    public float maxSpeed;
    public float minSize = .5f;
    public float maxSize = 2f;
    public float distanceFromEdge = 1f; //How far from the edge to spawn an asteroid
    public float offsetRange = .2f;
    public float asteroidWrapDistance = 1.5f;
    public GameObject asteroidPrefab;
    //public int maxAsteroids = 20;
    public int score = 0;
    public Text scoreText;
    int lastScore = 0;

    int numAsteroids = 0;
    //Potential starting positions for the asteroid
    enum Position { Above, Below, Left, Right };
	// Use this for initialization
	void Start () {
        StartCoroutine(SpawnAsteroids());
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        scoreText.text = "Score: 0";
	}
	
	// Update is called once per frame
	void Update () {
	    if (score != lastScore)
        {
            lastScore = score;
            scoreText.text = "Score: " + score;
        }
	}

    IEnumerator SpawnAsteroids()
    {
        while (true)
        {
            /*
            if (numAsteroids >= maxAsteroids)
            {
                //If we have too many asteroids, wait spawnWait secs and try again
                yield return new WaitForSeconds(spawnWait);
                continue;
            }*/
            //Pick a random location, size, speed and direction
            float size = Random.Range(minSize, maxSize);
            float speed = Random.Range(minSpeed, maxSpeed);
            Position position = getRandomPosition();
            float xPos, zPos;
            getPosition(position, out xPos, out zPos, xBoundary, zBoundary);
            Vector3 direction = getDirection(position, xPos, zPos);

            //Spawn asteroid and send it in that direction by applying a force
            GameObject asteroid = Instantiate(asteroidPrefab, new Vector3(xPos, 0, zPos), Quaternion.identity);
            AsteroidControl astControl = asteroid.GetComponent<AsteroidControl> ();
            astControl.xBoundary = xBoundary;
            astControl.zBoundary = zBoundary;
            astControl.xWrapDistance = astControl.zWrapDistance = 1.5f;

            switch (position)
            {
                case Position.Above:
                    astControl.wallToIgnore = "FrontWall";
                    break;
                case Position.Below:
                    astControl.wallToIgnore = "BackWall";
                    break;
                case Position.Left:
                    astControl.wallToIgnore = "LeftWall";
                    break;
                case Position.Right:
                    astControl.wallToIgnore = "RightWall";
                    break;
            }


            Transform astTransform = asteroid.GetComponent<Transform>();
            astTransform.localScale = new Vector3(size, size, size);
            Rigidbody astRb = asteroid.GetComponent<Rigidbody>();
            astRb.AddForce(direction * speed);
            numAsteroids++;
            yield return new WaitForSeconds(spawnWait);
        }
    }

    //Get the direction the asteroid will go
    //Will randomly decide to go in a straight line or toward the middle + or - some small offset
    //Returns anormalized vector
    Vector3 getDirection(Position pos, float xPos, float zPos)
    {
        Vector3 direction;

        //Go toward the middle
        if (Random.Range(0,2) == 0)
        {
            direction = new Vector3(-xPos, 0, -zPos);
        } else //go straight toward the field
        {
            if (pos == Position.Above)
            {
                direction = new Vector3(0, 0, -1);
            } else if (pos == Position.Below)
            {
                direction = new Vector3(0, 0, 1);
            } else if (pos == Position.Left)
            {
                direction = new Vector3(1, 0, 0);
            } else
            {
                direction = new Vector3(-1, 0, 0);
            }
        }

        //Add a small offset
        direction += new Vector3(Random.Range(-offsetRange, offsetRange), 0, 
            Random.Range(-offsetRange, offsetRange));

        return direction.normalized;
    }

    Position getRandomPosition()
    {
        Position[] arr = { Position.Above, Position.Below, Position.Left, Position.Right };
        return arr[Random.Range(0, 4)];
    }

    void getPosition(Position pos, out float xPos, out float zPos, float xBoundary, float zBoundary)
    {
        if (pos == Position.Above)
        {
            xPos = Random.Range(-xBoundary, xBoundary);
            zPos = zBoundary + distanceFromEdge;
        } else if (pos == Position.Below)
        {
            xPos = Random.Range(-xBoundary, xBoundary);
            zPos = -zBoundary - distanceFromEdge;
        } else if (pos == Position.Left)
        {
            zPos = Random.Range(-zBoundary, zBoundary);
            xPos = -xBoundary - distanceFromEdge;
        } else
        {
            zPos = Random.Range(-zBoundary, zBoundary);
            xPos = xBoundary + distanceFromEdge;
        }
    }

}
