﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBulletScript : MonoBehaviour {
    public float speed = 100;
    public float xBoundaryOfScreen = 21;
    public float zBoundaryOfScreen = 10;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += transform.forward * speed * Time.deltaTime;
        if (transform.position.x > Mathf.Abs(xBoundaryOfScreen) || transform.position.z > Mathf.Abs(zBoundaryOfScreen)){
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        //Destroy laser bullet if it collides with the boundary
        if (collider.gameObject.tag == "LeftWall" || collider.gameObject.tag == "RightWall" ||
            collider.gameObject.tag == "FrontWall" || collider.gameObject.tag == "BackWall")
        {
            Destroy(gameObject);
        }
    }
}
