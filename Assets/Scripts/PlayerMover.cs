﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMover : MonoBehaviour {
    float xAim = 0;
    float yAim = 0;
    float horizontal = 0;
    float vertical = 0;
    public float xBoundary;
    public float zBoundary;
    public float xWrapDistance;
    public float zWrapDistance;
    public GameObject frontWall;
    public GameObject backWall;
    public GameObject leftWall;
    public GameObject rightWall;
    Transform tf;
    public float pushForce = 10;
    public GameObject laserPrefab;
    Rigidbody rb;
    int counter = 0;
    public int framesBetweenShots = 20;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Mathf.Abs(tf.position.x) > xBoundary)
        {
            tf.position = new Vector3(-gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        }
        if (Mathf.Abs(tf.position.z) > zBoundary)
        {
            tf.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -gameObject.transform.position.z);
        }
    }

    void FixedUpdate()
    {
        xAim = CrossPlatformInputManager.GetAxis("AimX");
        yAim = CrossPlatformInputManager.GetAxis("AimY");
        horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        vertical = CrossPlatformInputManager.GetAxis("Vertical");

        //Left stick position adds force
        rb.AddForce(new Vector3(horizontal * pushForce, 0, vertical * pushForce));

        //If the the right stick is being dragged, shoot in that direction
        if (xAim != 0 && yAim != 0)
        {
            //Right stick position sets rotation
            transform.rotation = Quaternion.LookRotation(new Vector3(xAim, 0, yAim));

            //Spawn a laser prefab that moves in the direction you're facing
            if (counter == 0)
            {
               Instantiate(laserPrefab, transform.position, transform.rotation);
            }
            counter = (counter + 1) % framesBetweenShots;
        }
    }

    /*
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "LeftWall")
        {
            gameObject.transform.position = new Vector3(-(gameObject.transform.position.x-xWrapDistance), gameObject.transform.position.y, gameObject.transform.position.z);
        }
        else if (collider.gameObject.tag == "RightWall")
        {
            gameObject.transform.position = new Vector3(-(gameObject.transform.position.x + xWrapDistance), gameObject.transform.position.y, gameObject.transform.position.z);
        }
        else if (collider.gameObject.tag == "FrontWall")
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -gameObject.transform.position.z+zWrapDistance);
        }
        else if (collider.gameObject.tag == "BackWall")
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -gameObject.transform.position.z-zWrapDistance);
        }   
    }*/

 
}
