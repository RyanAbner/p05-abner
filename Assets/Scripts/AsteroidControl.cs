﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AsteroidControl : MonoBehaviour
{
    public float tumbleSpeed;
    public float xBoundary;
    public float zBoundary;
    public float xWrapDistance;
    public float zWrapDistance;
    public string cameFrom;
    public string wallToIgnore;
    public GameObject asteroidExplosion;
    public GameObject playerExplosion;
    public int maxChildren = 4;
    public float childPosOffset = .05f;
    public float childForceRange = 200f;
    public float minChildSize = 1;
    GameManager gm;
    Text gameOverText;
    bool restart = false;

    // Use this for initialization
    void Awake()
    {
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * tumbleSpeed;
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        GameObject canvas = GameObject.FindWithTag("UITextCanvas");
        //This is really really hacky.
        foreach (Text text in canvas.GetComponentsInChildren<Text>())
        {
            if (!text.text.Contains("Score"))
            {
                gameOverText = text;
            }
        }

    }
    /*
    void Update()
    {
        if (restart)
        {
            SceneManager.LoadScene("main");
        }
    }
*/
    void OnTriggerEnter(Collider collider)
    {
        string cTag = collider.gameObject.tag;
        if (cTag == wallToIgnore)
        {
            return;
        }
        if (cTag == "LeftWall")
        {
            wallToIgnore = "RightWall";
            gameObject.transform.position = new Vector3(-(gameObject.transform.position.x), gameObject.transform.position.y, gameObject.transform.position.z);
        }
        else if (cTag == "RightWall")
        {
            wallToIgnore = "LeftWall";
            gameObject.transform.position = new Vector3(-(gameObject.transform.position.x), gameObject.transform.position.y, gameObject.transform.position.z);
        }
        else if (cTag == "FrontWall")
        {
            wallToIgnore = "BackWall";
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -gameObject.transform.position.z);
        }
        else if (cTag == "BackWall")
        {
            wallToIgnore = "FrontWall";
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -gameObject.transform.position.z);
        }
        else if (cTag == "Player")
        {
            Instantiate(playerExplosion, collider.gameObject.transform.position, Quaternion.identity);
            Destroy(collider.gameObject);
            gameOverText.text = "You hit an asteroid and died!\nStarting over...";
            Invoke("RestartGame", 3f);
        }
        else if (cTag == "LaserBullet")
        {
            Instantiate(asteroidExplosion, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
            gm.score++;

        }
    }

    void RestartGame()
    {
        SceneManager.LoadScene("main");
    }
}
