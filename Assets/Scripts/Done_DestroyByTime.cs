﻿using UnityEngine;
using System.Collections;


//This is a script from the Unity Space Shooter tutorial.
public class Done_DestroyByTime : MonoBehaviour
{
	public float lifetime;

	void Start ()
	{
		Destroy (gameObject, lifetime);
	}
}
