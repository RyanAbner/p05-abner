A simple twin-stick shooter. Some assets (models, textures, sounds) borrowed from the Unity Space Shooter tutorial or other asset packs.
All scripts written by me, except Done_DestroyByTime.cs and most of the input code. 
Original music by me!